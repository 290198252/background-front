module.exports = {
  presets: ["@vue/app"],
  plugins: [
    [
      "import",
      {
        libraryName: "element-ui",
        libraryDirectory: "src/components"
      },
      "transform-runtime"
    ]
  ]
};
