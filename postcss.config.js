/*
 * @Author: your name
 * @Date: 2021-11-06 16:40:19
 * @LastEditTime: 2021-11-06 16:40:19
 * @LastEditors: your name
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \background-front\postcss.config.js
 */
module.exports = {
    plugins: {
        'autoprefixer': { browsers: 'last 5 version' }
    }
}