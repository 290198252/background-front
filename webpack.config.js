var webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = {
	entry: './main.js',
	mode: 'development',
	module: {
		rules: [
			{
			test: /\.vue$/,
			loader: 'vue-loader'
			}
		]
	},
	plugins: [
		new VueLoaderPlugin()
	]
}