### 程序员的个人博客后台前端界面
本博客系统设计目标针程序员群体，提供了传统的文章管理，新增了个人时间管理,计划管理管理等功能，此外代码,数据库设计全部开源。

前台展示界面git地址: https://gitee.com/290198252/api


后台管理后端接口的git地址: https://gitee.com/290198252/background


后台管理前端界面的git地址: https://gitee.com/290198252/background-front

前台采样php 语言编写，后台接口采用golang编程，后台界面采用 js + vue框架编程。
存储方面使用的是 mysql + redis + elasticsearch。

本系统经过简单修改，可以修改为公司文档管理系统和人员任务管理系统。
### 怎么使用
部署：npm run alpha
单机测试: npm run serve

### 实现功能
注意单机测试情况下和服务器的跨域请求
现有功能：    
#### 简单用户认证

#### 用户管理
![image.png](https://www.testingcloud.club/sapi/api/image_download/f1d2f2c5-814c-11eb-8238-525400dc6cec.png)
#### 文章管理  
![image.png](https://www.testingcloud.club/sapi/api/image_download/a5f73ebf-814c-11eb-8238-525400dc6cec.png)
#### 文章修改
![image.png](https://www.testingcloud.club/sapi/api/image_download/a5f73ebf-814c-11eb-8238-525400dc6cec.png)
#### 每日时间管理
![image.png](https://www.testingcloud.club/sapi/api/image_download/66ebb116-744c-11eb-80c3-525400dc6cec.png)
#### 待办事项管理
![image.png](https://www.testingcloud.club/sapi/api/image_download/fa818b24-80eb-11eb-93e9-525400dc6cec.png)

#### 完成计划管理/统计


#### 时间周月统计

