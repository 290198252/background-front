import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
const store =  new Vuex.Store({
    namespace:false,
    state: {
        account:{},
        count:0,
    },
    mutations: {
        increament: state => {
            state.count ++
        },
        decrement: state => {
            state.count --
        },
        setAccount (state,account){
            state.account = account
        }
    },
    actions: {
    },
    getters:{
        userInfo: state =>{
            return state.account;
        }
    }
});
export default store;
