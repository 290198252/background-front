import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);
/* eslint-disable */
export default new Router({
  mode: 'history',
  base: process.env.NODE_ENV === 'production'? "/dist/admin/index.html#":'/',
  routes: [
    {
        path:'/login',
        name:'login',
        component: (resolve) => require(['./views/Login'],resolve),
    },
    {
        path: '/BarIview',
        name: '基础功能',
        component: (resolve) => require(['./views/SideBarView.vue'],resolve),
        children:[
            {
                path: "template_manager",
                name: "模板管理",
                component: (resolve) => require(['./components/templateManager'], resolve),
                meta: {
                    keepAlive: true, //此组件不需要被缓存
                }
            },
            {
                path: "memo_manager",
                name: "备忘录管理",
                component: (resolve) => require(['./components/memoManage'], resolve),
                meta: {
                    keepAlive: true, //此组件不需要被缓存
                }
            },
            {
                path: "plan_manager",
                name: "计划管理",
                component: (resolve) => require(['./components/planManager'], resolve),
                meta: {
                    keepAlive: true, //此组件不需要被缓存
                }
            },
            {
                path: "todo_read",
                name: "办事必先看",
                component: (resolve) => require(['./components/todo_read'], resolve),
                meta: {
                    keepAlive: true, //此组件不需要被缓存
                }
            },
            {
                path: "undo_project",
                name: "代办事项",
                component: (resolve) => require(['./components/undo'], resolve),
                meta: {
                    keepAlive: true, //此组件不需要被缓存
                }
            },
            {
                path: "articleManageTree",
                name: "文章树管理",
                component: (resolve) => require(['./components/articleManageTree.vue'], resolve)
            }

        ],
        meta: {
          keepAlive: true ,// 此组件需要被缓存
          isBack: false
        }
    },
    {
        path: '/BarIview',
        name: '二期功能',
        component: (resolve) => require(['./views/SideBarView.vue'],resolve),
        children:[
            {
                path:"bugManager",
                name:"bug管理",
                component: (resolve) =>  require(['./components/bug_manager.vue'],resolve),
                meta:
                {
                    keepAlive: true, //此组件不需要被缓存
                }
            },
            {
                path:"hardwareManager",
                name:"硬件查询",
                component: (resolve) =>  require(['./components/hardware_manager.vue'],resolve),
                meta:
                {
                        keepAlive: true, //此组件不需要被缓存
                }
            },
            {
                path:"hardwareAdd",
                name:"添加硬件",
                component: (resolve) =>  require(['./components/hardwareAdd.vue'],resolve),
                meta:
                    {
                        keepAlive: true, //此组件不需要被缓存
                    }
            },
            {
                path:"needsAdd",
                name:"需求管理",
                component: (resolve) =>  require(['./components/bugAdd.vue'],resolve),
                meta:
                    {
                        keepAlive: true, //此组件不需要被缓存
                    }
            },
            {
                path:"fileManager",
                name:"文件管理",
                component: (resolve) =>  require(['./components/filemanager.vue'],resolve),
                meta:
                    {
                        keepAlive: true, //此组件不需要被缓存
                    }
            },
            {
                path:"bookManager",
                name:"书籍管理",
                component: (resolve) =>  require(['./components/template.vue'],resolve),
                meta:
                    {
                        keepAlive: true, //此组件不需要被缓存
                    }
            },
            {
                path:"study_manager",
                name:"学习管理",
                component: (resolve) =>  require(['./components/book.vue'],resolve),
                meta:
                    {
                        keepAlive: true, //此组件不需要被缓存
                    }
            }
        ]
      },
        {
              path: "/article/:id",
              name: "articleShow",
              component:  (resolve) => require(['./views/articleShow.vue'],resolve),
              meta: {
                keepAlive: true, //此组件不需要被缓存
              }
        },
    {
        path:'/monitor',
        name:'monitor',
        component: (resolve) => require(['./views/VideoPlayer'],resolve),
    },
  ]
})
