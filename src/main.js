/*
 * @Author: your name
 * @Date: 2019-05-25 02:08:04
 * @LastEditTime: 2021-11-06 17:04:29
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \background-front\src\main.js
 */
import Vue from 'vue';

import Button from "element-ui/lib/button";
import Dialog from "element-ui/lib/dialog";
import Form from "element-ui/lib/form";
import FormItem from "element-ui/lib/form-item";
import Input from "element-ui/lib/input";
import CheckBox from "element-ui/lib/checkbox";
import Card from "element-ui/lib/card"
import Select from "element-ui/lib/select"
import Table from 'element-ui/lib/table';
import ELTableColumn from 'element-ui/lib/table-column';
import  BreadCrumb from 'element-ui/lib/breadcrumb'
import Options from "element-ui/lib/option"
import  Radio from "element-ui/lib/radio"
import  Upload from "element-ui/lib/upload"
import DatePicker  from "element-ui/lib/date-picker"
import Drawer from "element-ui/lib/drawer"
import Message from "element-ui/lib/message"
import MessageBox from "element-ui/lib/message-box"
import Pagination from "element-ui/lib/pagination"
import  Calendar from "element-ui/lib/calendar"
import TimeLine from "element-ui/lib/timeline"
import TimeLineItem from "element-ui/lib/timeline-item"
import TimeSelect from "element-ui/lib/time-select"
import Tree from "element-ui/lib/tree"

import "element-ui/lib/theme-chalk/index.css";
import "./plugins/axios";
import App from "./App.vue";
import router from "./router";
import Router from "vue-router";
import Axios from "axios";
import store from "./store/index.js";
import "mavon-editor/dist/css/index.css";
import MavonEditor from 'mavon-editor'

import global from "@/store/global";

console.log(MavonEditor)
Vue.use(Tree);
Vue.use(TimeSelect);
Vue.use(TimeLineItem);
Vue.use(TimeLine);
Vue.use(Calendar);
Vue.use(Message);
Vue.use(MessageBox);
Vue.use(Drawer);
Vue.use(BreadCrumb);
Vue.use(ELTableColumn);
Vue.use(Upload);
Vue.use(Table);
Vue.use(Pagination);
Vue.use(Select);
Vue.use(Options);
Vue.use(Radio);
Vue.use(DatePicker);
Vue.use(MavonEditor);
Vue.use(Input);
Vue.use(CheckBox);
Vue.use(Button);
Vue.use(Dialog);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Router);
Vue.use(Axios);
Vue.use(Card);

Vue.prototype.Global = global

router.beforeEach((to,from,next)=>{
  window.addEventListener('load',function () {
    let x = from.hash.replace(to.hash,"")
    x = x.replace("#","")
    router.push(x)
  })
  next();
})
new Vue({
  render: h => h(App),
  el: "#app",
  store: store,
  router: router
}).$mount("#app");
