let utils = {}

utils.getDateFormat = function () {
    var date = new Date();
//年
    var year= date.getFullYear();
//月
    var month = date.getMonth()+1;
    if(month < 10){
        month = "0" + month
    }
//日
    var day = date.getDate();
    if(day < 10){
        day = "0" + day
    }
//时
    var hh = date.getHours();
    if(hh < 10){
        hh = "0" + hh
    }
//分
    var mm = date.getMinutes();
    if(mm < 10){
        mm = "0" + mm
    }
//秒
    var ss = date.getSeconds();
    if(ss < 10){
        ss = "0" + ss
    }
    var rq = year + "-" + month + "-" + day + " "+ hh + ":" + mm + ":"+ss;
    return rq;
}
export default utils;
