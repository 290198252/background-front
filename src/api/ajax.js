import axios from 'axios'
import store from '@/store/index.js'

/**
 * 获取一个新的自定义的axios实例
 */
const ajaxUrl =
    process.env.NODE_ENV === 'development'
        ? "http://127.0.0.1:4596/api/" // 开发环境中的后端地址
        : process.env.NODE_ENV === 'production'
        ? "https://www.testingcloud.club/sapi/api" // 生产环境中的后端地址
        : window.config.Host.test // 测试环境的后端地址
let ajax = axios.create({
    baseURL: ajaxUrl,
    timeout: 30000,
    withCredentials: true,
    async:true,
    crossDomain:true,
});

ajax.interceptors.request.use(function (xhr) {
    return xhr;
}, function (error) {
    console.log(error);
    // 对请求错误做些什么
    return Promise.reject(error);
});
ajax.interceptors.response.use(function(response) {
    console.log("response is :",response)
    return response
}, function(error) {
    // Vue.$vux.loading.hide()
    console.log(error);
    return Promise.reject(error)
});
export default ajax;
