/*
 * @Author: your name
 * @Date: 2019-05-23 22:39:12
 * @LastEditTime: 2021-08-08 22:59:30
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \background-front\Vue.config.js
 */
// vue.config.js
const CompressionPlugin = require("compression-webpack-plugin")
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
    // 修改的配置
    // 部署应用时的基本 URL。默认情况下，
    //如果应用被部署在一个子路径上，你就需要用这个选项指定这个子路径。
    //则设置 baseUrl 为 /my-app/
    publicPath:process.env.NODE_ENV === 'production'
        ? './'
        : '/',
    devServer: {
            host:"127.0.0.1",
            port:8080,
            useLocalIp: true,
            //proxy: 'http://127.0.0.1:8080',
    },
    configureWebpack: config => {
        console.log("process.env.NODE_ENV: ",process.env.NODE_ENV )
        if (process.env.NODE_ENV === 'production') {
             // 代码压缩 
             config.plugins.push( new UglifyJsPlugin({ 
                 uglifyOptions: { //生产环境自动删除
                        compress: { 
                            drop_debugger: true,
                            drop_console: true, 
                            pure_funcs: ['console.log'] 
                        } 
                    }, 
                    sourceMap: false, 
                    parallel: true }
                    )
                )
        }
    }
    ,
    chainWebpack:  config => {
        // GraphQL Loader
        config.module
            .rule('graphql')
            .test(/\.graphql$/)
            .use('graphql-tag/loader')
            .loader('graphql-tag/loader')
            .end();
        config.output.filename('[name].bu.js');
        config.output.chunkFilename ('[name].module.js');
        config.optimization.splitChunks({
            chunks: 'all'
        });
        config.entry('main').add('babel-polyfill')
        // 开启图片压缩
        config.module.rule('images')
        .test(/\.(png|jpe?g|gif|svg)(\?.*)?$/)
        .use('image-webpack-loader')
        .loader('image-webpack-loader')
        .options({ bypassOnDebug: true })
        // 开启js、css压缩
        if (process.env.NODE_ENV === 'production') {
          config.plugin('compressionPlugin')
          .use(new CompressionPlugin({
            test:/\.js$|\.html$|\.css/, // 匹配文件名
            threshold: 10240, // 对超过10k的数据压缩
            deleteOriginalAssets: false // 不删除源文件
          }))
        }
    },
    transpileDependencies: [
        'biyi-admin', // 指定对第三方依赖包进行babel-polyfill处理
    ]
// 为开发环境修改配置...
}
